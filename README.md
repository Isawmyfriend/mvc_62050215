## Common setup

Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/Isawmyfriend/mvc_62050215.git
```

```bash
npm install
```
## Getting Start

First, run the development server:

```bash
npm run dev
```

## Model
โฟลเดอร์ Model จะมีไฟล์ที่ชื่อว่า fileModel.js โดยในไฟล์นั้นจะประกอบด้วย 2 ฟังก์ชั่น
### Function in fileModel.js
1. uploadFile()
ทำหน้าที่รับค่า Path ของไฟล์นั้นมาและ Random ค่า Read, Like, Share, Comment และเก็บลงฐานข้อมูล 
1. calculateEngagement()
ทำหน้าที่ดึงข้อมูลมาจากฐานข้อมูลและนำค่า Read, Like, Share, Comment เหล่านั้นมาคำนวณ Engagement เพื่อดูว่ารูปนั้นเป็น Viral หรือไม่ และอีกหน้าที่คือ เพิ่มข้อมูล Engagement, Viral ลงไปในข้อมูลที่ดึงมาจากฐานข้อมูลก่อนจะนำไปแสดงผล


## Controller
โฟลเดอร์ Controller จะมีไฟล์ที่ชื่อว่า fileController.js โดยในไฟล์นั้นจะประกอบด้วย 2 ฟังก์ชั่น
### Function in fileController.js
1. uploadFile()
ทำหน้าที่รับค่า Path ของไฟล์นั้นมาจาก View และคืนค่า message กับ status ออกไปให้กับ View
1. calculateEngagement()
คืนค่า message, status และ ข้อมูลที่ดึงมากจากฐานข้อมูลโดยเพิ่มค่า Engagement กับ Viral ให้กับข้อมูลแล้วและส่งออกไปให้ View

## View
SwaggerUI ใช้ HTTP request methods เพื่อใช้งาน ฟังก์ชั่นใน Controller

