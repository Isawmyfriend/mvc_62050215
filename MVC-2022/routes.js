const fileController = require("./controller/fileController");

module.exports = (app) => {
  //Set path for Upload File
  app.post("/uploadFile", async (req, res) => {
    //Get data from Body
    const data = req.body;

    const { status, message } = await new fileController().uploadFile(data);

    res.status(status).json(message);
  });

  //Set path for Calculate Engagement
  app.get("/calculateEngagement", async (req, res) => {
    const { status, message, data } =
      await new fileController().calculateEngagement();

    res.status(status).json({ message: message, data: data });
  });
};
