const fileModel = require("../model/fileModel");

class fileController {
  async uploadFile(data) {
    //Call uploadFile Function from Model
    const { message, error } = await new fileModel().uploadFile(data);

    //Check Error from Call uploadFile Function and Return Value
    if (error) {
      return { status: 400, message: message };
    } else {
      return { status: 200, message: message };
    }
  }

  async calculateEngagement() {
    //Call calculateEngagement Function from Model
    const { message, error, data } =
      await new fileModel().calculateEngagement();

    //Check Error from Call calculateEngagement Function and Return Value
    if (error) {
      return { status: 400, message: message, data: data };
    } else {
      return { status: 200, message: message, data: data };
    }
  }
}

module.exports = fileController;
