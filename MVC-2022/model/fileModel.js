const db = require("../DB");

class fileModel {
  async uploadFile(data) {
    //Random Read, Like, Share, Comment
    let File_Read = this.randomNumber(100000, 1000);

    //Like, Share, Comment must be less that Read (Max = File Read, Min = 1)
    let File_Like = this.randomNumber(File_Read, 1);
    let File_Share = this.randomNumber(File_Read, 1);
    let File_Comment = this.randomNumber(File_Read, 1);

    //Connect Database
    await db.open();

    //Insert Data to Database
    const { error } = await db.query(
      "INSERT INTO meme_image_file VALUES (?,?,?,?,?)",
      [data.File_Path, File_Read, File_Like, File_Share, File_Comment]
    );

    //Disconnect Database
    await db.close();

    //Check Error from Insert Data to Database
    if (error) {
      return { message: "Error to upload File", error: true };
    } else {
      return { message: "Upload File Complete", error: false };
    }
  }

  async calculateEngagement() {
    //Connect Database
    await db.open();

    //Select Data from Database
    const { rows, error } = await db.query("SELECT  * FROM meme_image_file");

    //Disconnect Database
    await db.close();

    //For loop to get All Data to Calculate Engagement
    for (var i = 0; i < rows.length; i++) {
      //Calculate Engagement from Data
      let engagement =
        (rows[i].File_Like + rows[i].File_Share + rows[i].File_Comment) /
        rows[i].File_Read;

      //Check Image is Viral or Not
      if (engagement < 0.1) {
        //Add Engagement value and Viral value to Data
        Object.assign(rows[i], { Engagement: engagement, Viral: false });
      } else {
        Object.assign(rows[i], { Engagement: engagement, Viral: true });
      }
    }

    //Check Error from Select Data from Database and Return Value
    if (error) {
      return {
        message: "Error to Calculate Engagement",
        error: true,
        data: [],
      };
    } else {
      return {
        message: "Calculate Engagement Complete",
        error: false,
        data: rows,
      };
    }
  }

  //Random Number Function
  randomNumber(max, min) {
    return Math.floor(Math.random() * (max - min) + min);
  }
}

module.exports = fileModel;
