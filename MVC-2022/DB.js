require("dotenv").config();
const mysql = require("mysql2/promise");

class DB {
  //Set Config to Connect Database
  static async open() {
    this.db = await mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
    });
  }

  //Disconnect Database
  static async close() {
    await this.db.end();
  }

  //Create query function to query Data from Database
  static async query(queryString, values = []) {
    try {
      const [rows, fields] = await this.db.query(queryString, values);
      return { rows, fields, error: false };
    } catch (error) {
      console.error(error);
      return { error: true };
    }
  }
}

module.exports = DB;
